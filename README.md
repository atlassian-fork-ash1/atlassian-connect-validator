# Atlassian Connect Validator

Provides utility functions to validate an Atlassian Connect descriptor against a schema.

### Usage

```
var validator = require('atlassian-connect-validator');

validator.validateDescriptor(descriptor, schema, function (errors) {
    if (errors) {
        console.log(errors);
    } else {
        console.log("validation passed");
    }
});
```